import { ISubscription } from 'src/app/core/models/subscription.model';
import { IUserState, initialUserState } from './user.state';

export interface IAppState {
  user: IUserState;
}

export const initialAppState: IAppState = {
  user: initialUserState,
};

export function getInitialState(): IAppState {
  return initialAppState;
}
