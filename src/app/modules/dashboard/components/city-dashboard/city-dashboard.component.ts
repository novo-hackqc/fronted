import { Component, NgZone, ViewChild, ElementRef } from '@angular/core';
import { BorneServiceService } from 'src/app/core/services/borne-service.service';
import { IBorne } from 'src/app/core/models/Borne';
import { IReport } from 'src/app/core/models/report';
import { ReportService } from 'src/app/core/services/report.service';
import { MapsAPILoader } from '@agm/core';
import { IStats } from 'src/app/core/models/IStats';
import { CityStatsService } from 'src/app/core/services/city-stats.service';
import { error } from 'protractor';
import { reportType } from '../report-dialog/report-dialog.component';

@Component({
  selector: 'app-city-dashboard',
  templateUrl: './city-dashboard.component.html',
  styleUrls: ['./city-dashboard.component.scss'],
})
export class CityDashboardComponent {
  public bornes: IBorne[];
  displayedBornesColumns: string[] = ['id', 'waterLevel', 'status', 'action'];
  public reports: IReport[];
  displayedReportsColumns: string[] = ['type','id', 'description', 'Date', 'action'];
  public geoCoder;
  public currgeocoder;
  public google: any;
  public bornId: number;
  public reportId: number;
  public address: string;
  public isSending: boolean;
  public isSucess: boolean;
  public isEchec: boolean;

  public radius: number;
  public textToSend: string;
  private readonly defaultZoom = 18;

  @ViewChild('search', { static: false })
  public searchElementRef: ElementRef;
  zoom = this.defaultZoom;
  mapLat: number = 0;
  mapLng: number = 0;

  public stats: IStats = {
    devicesCount: 0,
    floodedAddresses: 0,
    floodedPeople: 0,
    peopleRequestingHelp: 0,
    floodedArea: 0,
  };

  constructor(
    private borneService: BorneServiceService,
    private reportService: ReportService,
    private mapsAPILoader: MapsAPILoader,
    private statsService: CityStatsService,
    private ngZone: NgZone,
  ) {
    this.bornes = [];
    this.reports = [];
    this.bornId = 0;
    this.reportId = 0;
    this.radius = null;
    this.textToSend = '';
    this.address = '';
    this.isSending = false;
    this.isSucess = false;
    this.isEchec = false;
  }

  async ngOnInit() {
    this.borneService.getBornes().then(result => {
      this.bornes = result;
    });
    this.reportService.getAllReports().then(result => {
      this.reports = result;
      console.log(result);
    });
    this.initSearchBar();

    this.stats = await this.statsService.getStats();
  }

  private initSearchBar() {
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder();
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address'],
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.mapLat = place.geometry.location.lat();
          this.mapLng = place.geometry.location.lng();
          this.zoom = this.defaultZoom;
        });
      });
    });
  }

  SendSms() {
    this.isSending = true;
    let sendObject = {
      coordinates: {
        lat: this.mapLat,
        lng: this.mapLng,
      },
      radius: Number(this.radius),
      text: this.textToSend,
    };
    this.statsService
      .sendSms(sendObject)
      .then(result => {
        this.isSending = false;
        this.isSucess = true;
        this.textToSend = null;
        this.radius = null;
        this.address = null;
      })
      .catch(error => {
        this.isSending = false;
        this.isEchec = true;
        console.log(error);
      });
  }

  deletereport(reportId) {
    this.reportService
      .delete(reportId)
      .then(result => {
        this.ngOnInit();
      })
      .catch(error => { });
  }

  getId(id) {
    return id.substring(1, 4);
  }

  getSchedule(date: Date) {
    date = new Date(date);
    return `${(date.getUTCHours() + 3) % 24}:${('0' + date.getMinutes()).slice(
      -2,
    )} ${date.getDate()}/${date.getMonth() + 1}/${date.getUTCFullYear()}`;
  }

  getCurrentLocation(lat, lng): string {
    return 'ok';
    console.log('resasd');
    this.mapsAPILoader.load().then(() => {
      let geocoder = new google.maps.Geocoder();
      let latlng = { lat: lat, lng: lng };
      let that = this;
      geocoder.geocode({ location: latlng }, results => {
        console.log('res:0', results);
        if (results[0]) {
          // that.zoom = 11;
          // that.currentLocation = results[0].formatted_address;
          console.log('res:', results[0].formatted_address);
        } else {
          console.log('No results found');
        }
      });
    });
  }

  public iconFromType(report: IReport): string {
    const type = report.eventType;
    switch (type) {
      case 2:
        return this.waterMarkersIcon.url;
      case 1:
        return this.roadMarkersIcon.url;
      case 3:
        return this.helpMarkersIcon.url;
      default:
        throw new Error('not supported');
    }
  }

  public waterMarkersIcon = {
    url: 'https://i.imgur.com/hiV78Ku.png',
    scaledSize: {
      width: 40,
      height: 40,
    },
    anchor: { x: 20, y: 20 },
  };

  public roadMarkersIcon = {
    url: 'https://i.imgur.com/yzegNyS.png',
    scaledSize: {
      width: 40,
      height: 40,
    },
    anchor: { x: 20, y: 20 },
  };

  public helpMarkersIcon = {
    url: 'https://i.imgur.com/HLEFjbm.png',
    scaledSize: {
      width: 40,
      height: 40,
    },
    anchor: { x: 20, y: 20 },
  };
}
