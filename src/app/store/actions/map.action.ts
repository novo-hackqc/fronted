import { Action } from '@ngrx/store';
import { ISubscription } from 'src/app/core/models/subscription.model';
import { Injectable } from '@angular/core';
import { IMapState } from 'src/app/core/models/IMapState';

export const ADD_MAP = '[ARTICLE] Add map';
export const UPDATE_MAP = '[ARTICLE] Update map';

export class AddMap implements Action {
  readonly type = ADD_MAP;
  constructor(public payload: IMapState) {}
}
export class UpdateMap implements Action {
  readonly type = UPDATE_MAP;
  constructor(public payload: IMapState) {}
}

export type Actions = AddMap | UpdateMap;
