import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BorneDialogComponent } from './borne-dialog.component';

describe('BorneDialogComponent', () => {
  let component: BorneDialogComponent;
  let fixture: ComponentFixture<BorneDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BorneDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BorneDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
