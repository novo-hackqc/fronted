import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class MapService {
  public apiEndpoint = environment.apiEndpoint //'http://prodloadbalancer-926743094.us-east-1.elb.amazonaws.com/api';

  constructor(private http: HttpClient) {}

  // public async getSubscribeByPhoneNumber(phoneNumber: string): Promise<ISubscription> {
  //   return new Promise(async (resolve, reject) => {
  //     try {
  //       const response = await this.http
  //         .get<ISubscription>(`${this.apiEndpoint}/get-subscribe/${phoneNumber}`)
  //         .toPromise();
  //       console.log(response);
  //       if (response) {
  //         resolve(response);
  //       } else {
  //         reject({ code: 'error.somethingWentWrong' });
  //       }
  //     } catch (incomingError) {
  //       reject(incomingError.error);
  //     }
  //   });
  // }
}
