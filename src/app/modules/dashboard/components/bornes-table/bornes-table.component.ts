import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTable } from '@angular/material/table';
import { BornesTableDataSource, BornesTableItem } from './bornes-table-datasource';
import { BorneServiceService } from 'src/app/core/services/borne-service.service';
import { IBorne } from 'src/app/core/models/Borne';
import { Bornes } from 'src/app/core/models/Bornes';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { CityStatsService } from 'src/app/core/services/city-stats.service';

@Component({
  selector: 'app-bornes-table',
  templateUrl: './bornes-table.component.html',
  styleUrls: ['./bornes-table.component.scss'],
})
export class BornesTableComponent implements AfterViewInit, OnInit {
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatTable, { static: false }) table: MatTable<BornesTableItem>;
  dataSource: IBorne[];

  /** Columns displayed in the table. Columns IDs can be added, removed, or reordered. */
  displayedColumns = ['status', 'deviceId', 'currentWaterLevel'];

  constructor(
    private BorneService: BorneServiceService,
    iconRegistry: MatIconRegistry,
    sanitizer: DomSanitizer,
  ) {
    this.dataSource = [];
    iconRegistry.addSvgIcon(
      'archive',
      sanitizer.bypassSecurityTrustResourceUrl(
        'https://raw.githubusercontent.com/google/material-design-icons/master/content/svg/production/ic_archive_24px.svg',
      ),
    );
  }

  async ngOnInit() {
    this.dataSource = await this.BorneService.getBornes();
    console.log(this.dataSource);
  }

  ngAfterViewInit() {
    //this.table.dataSource = this.dataSource;
  }
}
