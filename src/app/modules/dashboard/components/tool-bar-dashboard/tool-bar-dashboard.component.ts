import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { SideNavService } from '../../sideNav.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tool-bar-dashboard',
  templateUrl: './tool-bar-dashboard.component.html',
  styleUrls: ['./tool-bar-dashboard.component.scss'],
})
export class ToolBarDashboardComponent implements OnInit {
  title: string = 'Nov Eau';
  @Output() signInRouter = new EventEmitter<string>();
  @Output() openSideNav = new EventEmitter<boolean>();

  open: boolean;

  constructor(private sideNavService: SideNavService, public router: Router) {
    this.open = false;
  }

  ngOnInit() {}

  clickMenu() {
    //this.sideNavService.toggle();
    this.open = this.open === true ? false : true;
    this.openSideNav.emit(this.open);
  }
  navigateTo() {
    this.router.navigate(['/dashboard']);
  }

  signIn() {
    this.signInRouter.emit('/dashboard/map');
  }
}
