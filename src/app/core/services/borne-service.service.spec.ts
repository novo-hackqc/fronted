import { TestBed } from '@angular/core/testing';

import { BorneServiceService } from './borne-service.service';

describe('BorneServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BorneServiceService = TestBed.get(BorneServiceService);
    expect(service).toBeTruthy();
  });
});
