export interface IFlaque { locs: ILocation[]};

export interface ILocation {
    lat: number,
    lng: number;
}