import { Component, OnInit, forwardRef, EventEmitter, Output } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-sign-in-dashboard',
  templateUrl: './sign-in-dashboard.component.html',
  styleUrls: ['./sign-in-dashboard.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SignInDashboardComponent),
      multi: true,
    },
  ],
})
export class SignInDashboardComponent implements OnInit {
  public authentificationform: FormGroup;
  @Output()
  public handleSubmit = new EventEmitter<any>();

  public defaultValues: any;
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  constructor(public fb: FormBuilder, public router: Router) {
    this.defaultValues = {
      firstName: '',
      lastName: '',
      email: '',
      address: '',
      cellPhone: '',
      residentialPhone: '',
      password: '',
      confirmPassword: '',
      postalCode: '',
    };
  }

  ngOnInit() {
    this.authentificationform = this.initForm();
  }

  private initForm() {
    return this.fb.group({
      email: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  public register() {
    this.router.navigate(['/dashboard/Sing-up']);
  }

  public handleFormSubmit() {
    // if (
    //   this.authentificationform.value.email === 'admin@admin.com' &&
    //   this.authentificationform.value.password === '123456'
    // ) {
    //   this.router.navigate(['/dashboard/account']);
    // }
    this.router.navigate(['/dashboard/account']);
  }
}
