export interface IStats{
  devicesCount: number;
  floodedAddresses: number;
  floodedPeople: number;
  peopleRequestingHelp: number;
  floodedArea: number;
}
