import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IStats } from '../models/IStats';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class CityStatsService {
  constructor(private http: HttpClient) {}

  private readonly getStatsUrl = environment.apiEndpoint + '/Dashboard/Stats';
  public apiEndpoint = environment.apiEndpoint;

  public async getStats(): Promise<IStats> {
    return this.http.get<IStats>(this.getStatsUrl).toPromise();
  }

  public async sendSms(message): Promise<any> {
    console.log(message);
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http
          .post<any>(`${this.apiEndpoint}/Dashboard/SendSms`, message)
          .toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }
}
