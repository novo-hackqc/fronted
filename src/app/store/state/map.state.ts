import { IMapState } from 'src/app/core/models/IMapState';


export interface IMapStateState {
  map: IMapState;
  
}

export const initialMapState: IMapStateState = {
  map: null,
};
