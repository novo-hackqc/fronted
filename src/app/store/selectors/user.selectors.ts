import { IAppState } from '../state/app.state';
import { createSelector } from '@ngrx/store';
import { IUserState } from '../state/user.state';

const selectUser = (state: IAppState) => state.user;

export const selectCurrentUser = createSelector(selectUser, (state: IUserState) => state.user);
