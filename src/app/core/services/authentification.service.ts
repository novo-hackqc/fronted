import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISubscription } from '../models/subscription.model';

@Injectable({
  providedIn: 'root',
})
export class AuthentificationService {
  public apiEndpoint = 'https://v4dro1o5bi.execute-api.us-east-2.amazonaws.com/dev';

  constructor(private http: HttpClient) {}

  public create(user: ISubscription): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http.post<any>(`${this.apiEndpoint}/user/signup`, user).toPromise();

        if (response) {
          resolve(response);
          console.log(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public get(email: string, password: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http
          .post<any>(`${this.apiEndpoint}/user/sigin`, { email: email, password: password })
          .toPromise();

        if (response) {
          resolve(response);
          console.log(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }
}
