import { Injectable } from '@angular/core';
import { IFlaque } from '../models/Flaque';
import { environment } from 'src/environments/environment';
import { ISubscription, ICoordinates } from '../models/subscription.model';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DeviceService {
  public apiEndpoint = environment.apiEndpoint + "/devices/All" //'http://prodloadbalancer-926743094.us-east-1.elb.amazonaws.com/api';


  constructor(private http: HttpClient) { }

  public async getDevices(): Promise<Device[]> {


    return await this.http
      .get<Device[]>(this.apiEndpoint)
      .toPromise();


  }




}
export interface Device {
  deviceId: string,
  position: ICoordinates,
  // deviceStatus Status { get; set; }
  currentWaterLevel: number,
}
