import { Borne } from './Borne';
import { Bornes } from './Bornes';

export class Street {
  borneX: Borne;
  borneY: Borne;

  private constructor(borneX: Borne, borneY: Borne) {
    this.borneX = borneX;
    this.borneY = borneY;
  }

  public static buildStreet(borneIdX: string, borneIdY: string, bornes: Bornes): Street {
    const borneX = bornes.getBorne(borneIdX);
    const borneY = bornes.getBorne(borneIdY);
    return new Street(borneX, borneY);
  }

  public isBlocked(): boolean {
    return this.borneX.isFlooded() || this.borneY.isFlooded();
  }

  public getColor(): string {
    if (this.borneX.isRed() || this.borneY.isRed()) return 'red';
    else return '#ffe600';
  }
}
