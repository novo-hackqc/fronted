import { TestBed } from '@angular/core/testing';

import { FlaqueService } from './flaque.service';

describe('FlaqueService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FlaqueService = TestBed.get(FlaqueService);
    expect(service).toBeTruthy();
  });
});
