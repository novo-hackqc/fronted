import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HttpClientModule } from '@angular/common/http';

//services
import { AuthentificationService } from './services/authentification.service';
import { BorneServiceService } from './services/borne-service.service';
import { SubscriptionService } from './services/subscription.service';

@NgModule({
  declarations: [],
  imports: [CommonModule, HttpClientModule],
  providers: [AuthentificationService, BorneServiceService, SubscriptionService],
})
export class CoreModule {}
