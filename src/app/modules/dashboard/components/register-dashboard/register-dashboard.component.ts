import { Component, OnInit } from '@angular/core';
import { ISubscription } from 'src/app/core/models/subscription.model';
import { AuthentificationService } from 'src/app/core/services/authentification.service';
import { Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { AddUser } from 'src/app/store/actions/user.action';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register-dashboard',
  templateUrl: './register-dashboard.component.html',
  styleUrls: ['./register-dashboard.component.scss'],
})
export class RegisterDashboardComponent implements OnInit {
  constructor(
    private authentificationService: AuthentificationService,
    private store: Store<IAppState>,
    public router: Router,
  ) {}

  ngOnInit() {}

  register(newUser: ISubscription) {
    this.authentificationService
      .create(newUser)
      .then(result => {
        this.store.dispatch(new AddUser(newUser));
        this.router.navigate(['/dashboard/Sing-in']);
      })
      .catch(error => {
        console.log(error);
      });
  }
}
