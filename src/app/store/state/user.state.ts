import { ISubscription } from 'src/app/core/models/subscription.model';

export interface IUserState {
  user: ISubscription;
}

export const initialUserState: IUserState = {
  user: null,
};
