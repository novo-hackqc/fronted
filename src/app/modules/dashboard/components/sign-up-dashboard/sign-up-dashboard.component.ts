import { Component, OnInit, forwardRef, Output, EventEmitter } from '@angular/core';
import { NG_VALUE_ACCESSOR, FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

@Component({
  selector: 'app-sign-up-dashboard',
  templateUrl: './sign-up-dashboard.component.html',
  styleUrls: ['./sign-up-dashboard.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => SignUpDashboardComponent),
      multi: true,
    },
  ],
})
export class SignUpDashboardComponent implements OnInit {
  public registerform: FormGroup;

  @Output()
  public handleSubmit = new EventEmitter<any>();
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public defaultValues: any;
  emailFormControl = new FormControl('', [Validators.required, Validators.email]);

  constructor(public fb: FormBuilder) {
    this.defaultValues = {
      firstName: '',
      lastName: '',
      email: '',
      cellPhone: '',
      password: '',
      confirmPassword: '',
    };
  }

  ngOnInit() {
    this.registerform = this.initForm();
  }

  private initForm() {
    return this.fb.group({
      firstName: ['', Validators.required],
      lastName: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.required],
      password: ['', Validators.required],
      confirmPassword: ['', Validators.required],
    });
  }

  public handleFormSubmit() {
    this.handleSubmit.emit(this.registerform.value);
  }
}
