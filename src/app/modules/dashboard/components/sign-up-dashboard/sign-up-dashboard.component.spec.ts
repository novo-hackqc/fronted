import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SignUpDashboardComponent } from './sign-up-dashboard.component';

describe('SignUpDashboardComponent', () => {
  let component: SignUpDashboardComponent;
  let fixture: ComponentFixture<SignUpDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignUpDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SignUpDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
