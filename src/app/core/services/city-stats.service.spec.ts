import { TestBed } from '@angular/core/testing';

import { CityStatsService } from './city-stats.service';

describe('CityStatsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CityStatsService = TestBed.get(CityStatsService);
    expect(service).toBeTruthy();
  });
});
