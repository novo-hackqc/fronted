import { OnInit, Component, ElementRef, ViewChild, NgZone } from '@angular/core';
import { MapsAPILoader, PolyMouseEvent } from '@agm/core';
import { IAppState } from 'src/app/store/state/app.state';
import { Store } from '@ngrx/store';
import { MatDialog } from '@angular/material/dialog';
import { BorneDialogComponent } from '../borne-dialog/borne-dialog.component';
import { Borne, IMarker } from 'src/app/core/models/Borne';
import { BorneServiceService } from 'src/app/core/services/borne-service.service';
import { Bornes } from 'src/app/core/models/Bornes';
import { Street } from 'src/app/core/models/Street';
import { ActivatedRoute } from '@angular/router';
import { DialogComponent } from '../dialog/dialog.component';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
import { FormControl } from '@angular/forms';
import { ReportDialogComponent, reportType } from '../report-dialog/report-dialog.component';
import { ReportService } from 'src/app/core/services/report.service';
import { IFlaque } from 'src/app/core/models/Flaque';
import { FlaqueService } from 'src/app/core/services/flaque.service';
import { IReport } from 'src/app/core/models/report';
import { ReportInfoDialogComponent } from '../report-info-dialog/report-info-dialog.component';
import { Device, DeviceService } from 'src/app/core/services/device.service';

@Component({
  selector: 'app-map-dashboard',
  templateUrl: './map-dashboard.component.html',
  styleUrls: ['./map-dashboard.component.scss'],
})
export class MapDashboardComponent implements OnInit {
  private readonly pollingTime = 2000;

  private readonly defaultZoom = 18;

  public amgMarkIndex = 5;

  address: string;

  mapType = 'roadmap';
  zoom = this.defaultZoom;
  mapLat: number = 0;
  mapLng: number = 0;

  public searchControl: FormControl;

  public flaques: IFlaque[] = [];
  public reports: IReport[] = [];
  yourMarker: IMarker = { lat: 0, lng: 0 };
  bornes: Device[] = [];

  //marker: ReportMarker[] = [];

  //Points d'intérêt genre ta maison/chalet?
  interestMarkers: IMarker[] = [];

  public icon = {
    url: 'https://i.imgur.com/FQFIIYz.png',
    scaledSize: {
      width: 40,
      height: 40,
    },
  };

  public waterMarkersIcon = {
    url: 'https://i.imgur.com/hiV78Ku.png',
    scaledSize: {
      width: 40,
      height: 40,
    },
    anchor: { x: 20, y: 20 },
  };

  public roadMarkersIcon = {
    url: 'https://i.imgur.com/yzegNyS.png',
    scaledSize: {
      width: 40,
      height: 40,
    },
    anchor: { x: 20, y: 20 },
  };

  public helpMarkersIcon = {
    url: 'https://i.imgur.com/HLEFjbm.png',
    scaledSize: {
      width: 40,
      height: 40,
    },
    anchor: { x: 20, y: 20 },
  };
  streetViewControl = false;
  private geoCoder;

  @ViewChild('search', { static: false })
  public searchElementRef: ElementRef;
  pollingId: NodeJS.Timer;
  public currentDate: Date;

  constructor(
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    public dialog: MatDialog,
    private route: ActivatedRoute,
    public subscriptionService: SubscriptionService,
    public reportService: ReportService,
    private flaqueService: FlaqueService,
    //private deviceService:DeviceService
  ) {
    this.currentDate = new Date();
  }

  ngOnInit() {
    this.setMapToYourPosition();
    //create search FormControl
    this.searchControl = new FormControl();
    this.initSearchBar();

    this.startPolling();
    this.updateFlaque();
    this.updateReports();
    this.updateBorne();

  }

  async openDialog(address: string): Promise<void> {
    let info = await this.subscriptionService.getZoneInfo(this.mapLat, this.mapLng);
    const dialogRef = this.dialog.open(DialogComponent, {
      data: { address, lat: this.mapLat, lng: this.mapLng, info },
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
    });
  }

  private async updateFlaque() {
    this.flaques = await this.flaqueService.getFlaques();
  }
  private async updateBorne() {
    //this.bornes = await this.deviceService.getDevices();
    console.log("b: ", this.bornes);
  }
  private async updateReports() {
    //console.log(this.reportService);
    const newReports = await this.reportService.getAllReports();
    //  console.log("new reports: ", newReports);
    // const toRemove: IReport[] = [];

    this.reports.forEach(report => {
      for (let i = 0; i < newReports.length; i++) {
        if (report.eventId === newReports[i].eventId) {
          this.updateReport(report, newReports[i]);
          continue;
        }
      }
      //  toRemove.push(report);
    });
    // console.log("rem: ", toRemove)
    //this.reports.filter(obj => !toRemove.includes(obj));

    newReports.forEach(newReport => {
      if (!this.isIncluded(newReport)) {
        this.reports.push(newReport);
      }
    });
  }

  private updateReport(oldReport: IReport, newReport: IReport) {
    oldReport.comments = newReport.comments;
    oldReport.likes = newReport.likes;
  }

  private isIncluded(newReport: IReport): boolean {
    //console.log(this.reports);
    for (let i = 0; i < this.reports.length; i++) {
      //console.log(oldReport.eventId + "  " + newReport.eventId + "  " + (oldReport.eventId === newReport.eventId));
      if (this.reports[i].eventId === newReport.eventId) {
        return true;
      }
    }
    return false;
  }

  public hourFetching(index) {}

  public yearFetching(key) {
    //console.log(this.currentDate.get);
  }

  private startPolling() {
    this.pollingId = setInterval(() => {
      this.updateFlaque();
      this.updateReports();
    }, this.pollingTime);
  }

  ngOnDestroy() {
    if (this.pollingId) {
      clearInterval(this.pollingId);
    }
  }

  private initSearchBar() {
    this.mapsAPILoader.load().then(() => {
      this.geoCoder = new google.maps.Geocoder();
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ['address'],
        componentRestrictions:{country: ["CA"]}
      });
      autocomplete.addListener('place_changed', () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();

          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          this.mapLat = place.geometry.location.lat();
          this.mapLng = place.geometry.location.lng();
          this.zoom = this.defaultZoom;
          this.addInterestMarker(this.mapLat, this.mapLng);

          this.openDialog(this.getAddressFromPlace(place));
        });
      });
    });
  }

  private getAddressFromPlace(place: google.maps.places.PlaceResult): string {
    return place.formatted_address;
  }

  private setMapToYourPosition() {
    if (navigator.geolocation) {
      let self = this;
      navigator.geolocation.getCurrentPosition(
        function(position) {
          self.yourMarker.lat =  45.521358; //position.coords.latitude;
          self.yourMarker.lng = -73.860891; //position.coords.longitude;

          if (self.route.snapshot.params.lat && self.route.snapshot.params.lng) {
            const lat: number = parseFloat(self.route.snapshot.params.lat);
            const lng: number = parseFloat(self.route.snapshot.params.lng);
            self.mapLat = lat;
            self.mapLng = lng;
          } else {
            self.mapLat = self.yourMarker.lat;
            self.mapLng = self.yourMarker.lng;
          }
        },
        function() {
          this.handleLocationError();
        },
      );
    } else {
      // Browser doesn't support Geolocation
      this.handleLocationError();
    }
  }

  private handleLocationError() {
    Error('error');
  }

  private addBorneMarker(lat: number, lng: number, radius: number, color: string) {
    // this.borneMarkers.push({ lat, lng, alpha: 1 });
  }
  private addInterestMarker(lat: number, lng: number) {
    this.interestMarkers.push({ lat, lng });
  }

  selectReport(report: IReport) {
    const dialogRef = this.dialog.open(ReportInfoDialogComponent, {
      data: { report },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateReports();
    });
  }

  private OpenReportInfo(report: IReport) {}

  onMapReady(event) {
    const dialogRef = this.dialog.open(ReportDialogComponent, {
      disableClose: true,
      data: { lat: event.coords.lat, lng: event.coords.lng },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateReports();
    });
  }

  polyClick(event: PolyMouseEvent) {
    console.log('polygon', event);
    const dialogRef = this.dialog.open(ReportDialogComponent, {
      disableClose: true,
      data: { lat: event.latLng.lat, lng: event.latLng.lng },
    });

    dialogRef.afterClosed().subscribe(result => {
      this.updateReports();
    });
  }

  public iconFromType(type: reportType) {
    switch (type) {
      case 2:
        return this.waterMarkersIcon;
      case 1:
        return this.roadMarkersIcon;
      case 3:
        return this.helpMarkersIcon;
      default:
        throw new Error('not supported');
    }
  }
}
interface ReportMarker {
  lat: number;
  lng: number;
  icon: string;
}
