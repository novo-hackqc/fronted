import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ISubscription } from '../models/subscription.model';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class SubscriptionService {
  public apiEndpoint = environment.apiEndpoint //'http://prodloadbalancer-926743094.us-east-1.elb.amazonaws.com/api';

  constructor(private http: HttpClient) {}

  public async getSubscribeByPhoneNumber(phoneNumber: string): Promise<ISubscription> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http
          .get<ISubscription>(`${this.apiEndpoint}/get-subscribe/${phoneNumber}`)
          .toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public async subscribe(subscription: ISubscription): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http
          .post<any>(`${this.apiEndpoint}/Addresses/Subscribe`,  subscription )
          .toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public async getZoneInfo(lat: number, lng: number): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http
          .get<any>(`${this.apiEndpoint}/Addresses/Info?lat=${lat}&lng=${lng}`)
          .toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public async unsubscribe(userId: string, borneId: string): Promise<boolean> {
    //return api call
    return new Promise((resolve, reject) => {
      resolve(true);
    });
  }
}
