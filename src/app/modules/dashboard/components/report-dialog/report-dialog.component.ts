import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ReportService } from 'src/app/core/services/report.service';
import { IReport } from 'src/app/core/models/report';

@Component({
  selector: 'app-report-dialog',
  templateUrl: './report-dialog.component.html',
  styleUrls: ['./report-dialog.component.scss'],
})
export class ReportDialogComponent implements OnInit {
  readonly REPORT_STATE = reportType;
  public waterProblem: boolean;
  public roadProblem: boolean;
  public help: boolean;
  public currentState: reportType;
  public mapLat: number;
  public mapLng: number;
  public report: IReport;
  public description: string;
  public name: string;
  public phoneNumber: string;
  public isSending: boolean;
  public isSucess: boolean;
  public isEchec: boolean;

  constructor(
    public dialogRef: MatDialogRef<ReportDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public reportService: ReportService,
  ) {
    this.report = null;
    this.mapLng = data.lng;
    this.mapLat = data.lat;
    this.currentState = reportType.WATER;
    this.name = null;
    this.phoneNumber = null;
    this.description = null;
    this.isSending = false;
    this.isSucess = false;
    this.isEchec = false;
  }

  ngOnInit() {}

  water() {
    this.currentState = reportType.WATER;
  }

  closedRoad() {
    this.currentState = reportType.ROAD;
  }
  needHelp() {
    this.currentState = reportType.HELP;
  }

  handleFileInput(url) {
    console.log(url);
  }

  sendReport() {
    this.isSending = true;
    this.report = {
      createdDateTime: new Date(),
      eventId: '',
      eventType: this.currentState,
      coordinates: {
        lat: this.mapLat,
        lng: this.mapLng,
      },
      description: this.description,
    };
    switch (this.currentState) {
      case reportType.WATER:
        this.reportService
          .postNewReport(this.report)
          .then(result => {
            this.isSending = false;
            this.isSucess = true;
          })
          .catch(error => {
            this.isEchec = true;
            console.log(error);
          });
        break;
      case reportType.ROAD:
        this.report = {
          ...this.report,
          name: this.name,
          phoneNumber: this.phoneNumber,
        };
        this.reportService
          .postNewReport(this.report)
          .then(result => {
            this.isSending = false;
            this.isSucess = true;
          })
          .catch(error => {
            this.isEchec = true;
            console.log(error);
          });
        break;
      case reportType.HELP:
        this.report = {
          ...this.report,
          name: this.name,
          phoneNumber: this.phoneNumber,
        };
        this.reportService
          .postNewReport(this.report)
          .then(result => {
            this.isSending = false;
            this.isSucess = true;
          })
          .catch(error => {
            this.isEchec = true;
            console.log(error);
          });
        break;

      default:
        break;
    }
  }

  close(): void {
    this.dialogRef.close({
      state: this.currentState,
      lat: this.mapLat,
      lng: this.mapLng,
      description: this.description,
    });
  }
}

export enum reportType {
  WATER = 2,
  ROAD = 1,
  HELP = 3,
}
