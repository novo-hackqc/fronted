import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IReport } from '../models/report';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  public apiEndpoint = environment.apiEndpoint; //'http://prodloadbalancer-926743094.us-east-1.elb.amazonaws.com/api';

  constructor(private http: HttpClient) {}

  public async postNewReport(report: IReport): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http.post<any>(`${this.apiEndpoint}/Social/AddEvent`, report).toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public async likes(reportId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http
          .post<any>(`${this.apiEndpoint}/Social/${reportId}/Like`, {})
          .toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public async sendComment(comment: string, reportId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http
          .post<any>(`${this.apiEndpoint}/Social/${reportId}/AddComment`, { comment })
          .toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public async delete(reportId: string): Promise<any> {
    return new Promise(async (resolve, reject) => {
      try {
        const response = await this.http.delete<any>(`${this.apiEndpoint}/Social/${reportId}`).toPromise();
        console.log(response);
        if (response) {
          resolve(response);
        } else {
          reject({ code: 'error.somethingWentWrong' });
        }
      } catch (incomingError) {
        reject(incomingError.error);
      }
    });
  }

  public async getAllReports(): Promise<IReport[]> {
    return this.http.get<IReport[]>(`${this.apiEndpoint}/Social/All`).toPromise();
  }
}
