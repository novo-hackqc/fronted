import { reportType } from 'src/app/modules/dashboard/components/report-dialog/report-dialog.component';

export interface IReport {
  eventId: string;
  eventType: reportType;
  coordinates: ICoordinates;
  description: string;
  createdDateTime:Date;
  image?: string;
  phoneNumber?: string;
  name?: string;
  comments?: IComment[]
  likes?: number;
}

export interface IComment {
  comment: string;
}

export interface ICoordinates {
  lat: number;
  lng: number;
}
