import { Action } from '@ngrx/store';
import { ISubscription } from 'src/app/core/models/subscription.model';
import { Injectable } from '@angular/core';

export const ADD_USER = '[ARTICLE] Add user';
export const UPDATE_USER = '[ARTICLE] Update user';

export class AddUser implements Action {
  readonly type = ADD_USER;
  constructor(public payload: ISubscription) {}
}
export class UpdateUser implements Action {
  readonly type = UPDATE_USER;
  constructor(public payload: ISubscription) {}
}

export type Actions = AddUser | UpdateUser;
