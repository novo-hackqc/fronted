import { Injectable } from '@angular/core';
import { ISubscription } from '../models/subscription.model';
import { select, Store } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { selectCurrentUser } from 'src/app/store/selectors/user.selectors';
import { AddUser } from 'src/app/store/actions/user.action';
import { IMapState } from '../models/IMapState';

@Injectable({
  providedIn: 'root',
})
export class StorageService {
  // public currentUser$ = this.store.pipe(select(selectCurrentUser));

  //  public currentMap$ = this.store.pipe(select(selectCurrentMap));

  constructor(private store: Store<IAppState>) {}

  /*
  public async getUser(): Promise<IUser>{
    return await this.currentUser$.toPromise()
  }

  public async saveUser(user: IUser) {
    this.store.dispatch(new AddUser(user));
  }

  public async getMap(): Promise<IMapState>{
    return await this.currentUser$.toPromise()
  }

  public async saveMap(map :IMapState){
    
  }*/
}
