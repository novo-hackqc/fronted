import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { MapDashboardComponent } from './components/map-dashboard/map-dashboard.component';
import { AuthentificationDashboardComponent } from './components/authentification-dashboard/authentification-dashboard.component';
import { RegisterDashboardComponent } from './components/register-dashboard/register-dashboard.component';
import { CityDashboardComponent } from './components/city-dashboard/city-dashboard.component';

const routes: Routes = [
  {
    path: '',
    component: DashboardPageComponent,
    children: [
      {
        path: '',
        component: MapDashboardComponent,
      },
      {
        path: ':lat/:lng',
        component: MapDashboardComponent,
      },
      {
        path: 'Sing-in',
        component: AuthentificationDashboardComponent,
      },
      {
        path: 'Sing-up',
        component: RegisterDashboardComponent,
      },
      {
        path: 'account',
        component: CityDashboardComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashboardRoutingModule {}
