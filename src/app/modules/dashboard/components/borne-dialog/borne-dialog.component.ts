import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Borne } from 'src/app/core/models/Borne';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
import { Router } from '@angular/router';
import { ISubscription } from 'src/app/core/models/subscription.model';
import { StorageService } from 'src/app/core/services/storage.service';

@Component({
  selector: 'app-borne-dialog',
  templateUrl: './borne-dialog.component.html',
  styleUrls: ['./borne-dialog.component.scss'],
})
export class BorneDialogComponent implements OnInit {
  private readonly subscribeText = 'Subscribe';
  private readonly unsubscribeText = 'Unsubscribe';

  private user;
  buttonText: string;
  private subscribed: boolean;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public phoneNumber: string;
  public subscriptionLoading: boolean;
  public alreadyRegistered: boolean;

  constructor(
    public dialogRef: MatDialogRef<BorneDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public borne: Borne,
    private subscriptionService: SubscriptionService,
    private router: Router,
  ) {
    this.subscriptionLoading = false;
    this.alreadyRegistered = false;
  }

  ngOnInit(): void {
    this.user = undefined; //=getUser
  }

  private closeDialog() {
    this.dialogRef.close();
  }

  private navigateToSignIn() {
    this.router.navigate(['/dashboard/Sing-in']);
    this.closeDialog();
  }

  // private async borneSubscribe() {
  //   this.subscriptionLoading = true;
  //   this.subscriptionService
  //     .subscribe(this.phoneNumber, this.borne.id)
  //     .then(result => {
  //       this.subscriptionLoading = false;
  //       if (result.exists) {
  //         this.alreadyRegistered = true;
  //       }
  //     })
  //     .catch(error => {
  //       this.subscriptionLoading = false;
  //     });
  // }

  private async borneUnsubscribe(): Promise<boolean> {
    return this.subscriptionService.unsubscribe(this.user.uid, this.borne.id);
  }
}
