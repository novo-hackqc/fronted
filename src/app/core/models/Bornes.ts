import { Borne, IBorne } from './Borne';

export class Bornes {
    bornes: Borne[];

    constructor(bornes: IBorne[]) {
        this.bornes = [];

        bornes.forEach(borne => {
            this.bornes.push(new Borne(borne));
        });
    }

    public getBorne(borneId: string): Borne {
        // console.log(this.bornes);
        for (let i = 0; i < this.bornes.length; i++) {
            if (this.bornes[i].id === borneId) {
                // console.log("b: ", borneId, this.bornes[i]);
                return this.bornes[i];
            }
        }
        console.log('Borne not found: ' + borneId);
    }

    public updateBornesValue(newBornes: Bornes) {
        for (let i = 0; i < this.bornes.length; i++) {
            for (let j = 0; j < newBornes.bornes.length; j++) {
                if (this.bornes[i].id === newBornes.bornes[j].id) {
                    this.bornes[i].waterHeight = newBornes.bornes[j].waterHeight;
                }
            }

        }
    }
}
