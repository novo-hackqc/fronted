import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { SideNavService } from '../../sideNav.service';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Component({
  selector: 'app-dashboard-navigation',
  templateUrl: './dashboard-navigation.component.html',
  styleUrls: ['./dashboard-navigation.component.scss'],
})
export class DashboardNavigationComponent implements OnInit {
  public opened: boolean;
  constructor(private sideNavService: SideNavService, public router: Router) {
    this.opened = false;
  }

  ngOnInit() {}
  navigateTo(route) {
    this.router.navigate(['/dashboard/Sing-in']);
  }

  openSideNav() {
    this.opened = this.opened === false ? true : false;
  }
}
