import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthentificationDashboardComponent } from './authentification-dashboard.component';

describe('AuthentificationDashboardComponent', () => {
  let component: AuthentificationDashboardComponent;
  let fixture: ComponentFixture<AuthentificationDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthentificationDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthentificationDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
