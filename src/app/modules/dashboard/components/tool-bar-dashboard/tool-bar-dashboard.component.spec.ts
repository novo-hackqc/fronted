import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ToolBarDashboardComponent } from './tool-bar-dashboard.component';

describe('ToolBarDashboardComponent', () => {
  let component: ToolBarDashboardComponent;
  let fixture: ComponentFixture<ToolBarDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ToolBarDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ToolBarDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
