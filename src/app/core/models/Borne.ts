export class Borne implements IMarker {

    private readonly floodedThreshold = 0.25;
    private readonly yellowThreshhold = 0.1;

    id: string;
    name: string;
    waterHeight: number;

    lat: number;
    lng: number;





    constructor(json: any) {
        this.id = json.deviceId;
        //this.name = json.name;
        this.waterHeight = json.waterHeight;
        this.lat = json.lat;
        this.lng = json.lng;
    }

    //ark
    public waterHeightToString(): string {
        return Math.round(this.waterHeight * 100) + "cm"
    }

    public isFlooded() {
        return this.waterHeight >= this.yellowThreshhold


    }

    public isRed() {
        return this.waterHeight >= this.floodedThreshold;
    }

    public isYellow() {
        return this.waterHeight >= this.yellowThreshhold;
    }
}
export interface IBorne {
    deviceId: string;
    position: IMarker,
    currentWaterLevel: number
    status: number
}

export interface IMarker {
    lat: number;
    lng: number;
}

