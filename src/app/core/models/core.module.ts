import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// Modules

// Initializers

// Guards

// Interceptors

// Services

@NgModule({
  imports: [],
  providers: [],
})
export class CoreModule {}
