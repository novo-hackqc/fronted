import { Injectable } from '@angular/core';
import { Borne, IBorne } from '../models/Borne';
import { Street } from '../models/Street';
import { Bornes } from '../models/Bornes';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root',
})
export class BorneServiceService {
  private readonly getBornesUrl =
    'http://prodloadbalancer-926743094.us-east-1.elb.amazonaws.com/api/Devices/All?maxHistory=5';

  async getStreets(bornes: Bornes): Promise<Street[]> {
    /*const streets = await this.http.get<{ id1: string; id2: string }[]>(this.getStreetsUrl).toPromise();
    let response: Street[] = [];
    streets.forEach(street => {
      response.push(Street.buildStreet(street.id1, street.id2, bornes));
    });
    return response;*/
    return null;
    /*
    const bornes = await this.getBornes();
    return [

      /*Street.buildStreet("id1","id2",bornes),
      Street.buildStreet("id1","id5",bornes),

      Street.buildStreet("id2","id3",bornes),
      Street.buildStreet("id2","id6",bornes),

      Street.buildStreet("id3","id4",bornes),
      Street.buildStreet("id3","id7",bornes),

      Street.buildStreet("id5","id6",bornes),
      Street.buildStreet("id6","id7",bornes),

      Street.buildStreet("id7","id8",bornes),*/
    /*
      Street.buildStreet("id100","id101",bornes),
      Street.buildStreet("id101","id102",bornes),
      Street.buildStreet("id102","id103",bornes),
      Street.buildStreet("id103","id104",bornes),

      Street.buildStreet("id2000","id2005",bornes),
      Street.buildStreet("id2005","id2010",bornes),
      Street.buildStreet("id2010","id2015",bornes),
      Street.buildStreet("id2015","id2020",bornes),

      Street.buildStreet("id104","id2000",bornes),
      Street.buildStreet("id103","id2010",bornes),
      Street.buildStreet("id102","id2020",bornes),


    ]*/
  }

  constructor(private http: HttpClient) {}

  async getBornes(): Promise<IBorne[]> {
    const iBornes = await this.http.get<IBorne[]>(this.getBornesUrl).toPromise();
    return iBornes;
    /*
    return new Bornes([
      new Borne({ id: "id0", waterHeight: 100, lat: 46.543893, lng: -72.751874 }),
      new Borne({ id: "id1", waterHeight: 100, lat: 46.538291, lng: -72.750154 }),
      new Borne({ id: "id2", waterHeight: 100, lat: 46.538408, lng: -72.748082 }),
      new Borne({ id: "id3", waterHeight: 100, lat: 46.538504, lng: -72.745873 }),
      new Borne({ id: "id4", waterHeight: 100, lat: 46.538592, lng: -72.743757 }),
      new Borne({ id: "id5", waterHeight: 100, lat: 46.537935, lng: -72.750115 }),
      new Borne({ id: "id6", waterHeight: 100, lat: 46.538027, lng: -72.748060 }),
      new Borne({ id: "id7", waterHeight: 100, lat: 46.538119, lng: -72.745872 }),
      new Borne({ id: "id8", waterHeight: 100, lat: 46.538172, lng: -72.744270 }),
      //new Borne({id: "id9",  waterHeight: 10,lat: 46.543893, lng: -72.751874}),
      //new Borne({id: "id10",  waterHeight: 10,lat: 46.543893, lng: -72.751874}),
      new Borne({ id: "id100", waterHeight: 0, lat: 46.536960,lng: -72.745717}),
      new Borne({ id: "id101", waterHeight: 0, lat: 46.536735,lng: -72.746756}),
      new Borne({ id: "id102", waterHeight: 0, lat:46.536517,lng: -72.747824}),
      new Borne({ id: "id103", waterHeight: 100, lat:46.536425,lng: -72.748886}),
      new Borne({ id: "id104", waterHeight: 0, lat: 46.536300,lng: -72.749905}),

      new Borne({ id: "id2000", waterHeight: 0, lat: 46.536745,lng: -72.749979}),
      new Borne({ id: "id2005", waterHeight: 0, lat:46.536775,lng: -72.749422}),
      new Borne({ id: "id2010", waterHeight: 0, lat:46.536805,lng: -72.748900}),
      new Borne({ id: "id2015", waterHeight: 0, lat:46.536830,lng: -72.748373}),
      new Borne({ id: "id2020", waterHeight: 0, lat: 46.536843,lng: -72.747853}),
    ])*/
  }
}
/*
46.538291, -72.750154
46.538408, -72.748082
46.538504, -72.745873
46.538592, -72.743757
46.537935, -72.750115
46.538027, -72.748060
46.538119, -72.745872
46.538172, -72.744270*/
