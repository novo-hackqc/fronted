import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { IReport } from 'src/app/core/models/report';
import { reportType } from '../report-dialog/report-dialog.component';
import { ReportService } from 'src/app/core/services/report.service';

@Component({
  selector: 'app-report-info-dialog',
  templateUrl: './report-info-dialog.component.html',
  styleUrls: ['./report-info-dialog.component.scss'],
})
export class ReportInfoDialogComponent implements OnInit {
  readonly REPORT_STATE = reportType;
  public report: any;
  public reportState: reportType;
  public comment: string;
  public isSending: boolean;
  public isSucess: boolean;
  public isEchec: boolean;
  public likeNumber: number;

  constructor(
    public dialogRef: MatDialogRef<ReportInfoDialogComponent>,
    public reportService: ReportService,
    @Inject(MAT_DIALOG_DATA) public data: any,
  ) {
    this.report = this.data.report;
    this.isSending = false;
    this.isSucess = false;
    this.isEchec = false;
    this.likeNumber = this.report.likes;
    console.log(this.report);
    this.reportState = this.report.eventType;
  }

  ngOnInit() {}

  sendComment() {
    this.isSending = true;
    this.reportService
      .sendComment(this.comment, this.report.eventId)
      .then(result => {
        this.isSending = false;
        this.isSucess = true;
      })
      .catch(error => {
        this.isSending = false;
        this.isEchec = true;
      });
  }

  getSchedule(date: Date) {
    date = new Date(date);
    return `${(date.getUTCHours() + 3) % 24}:${('0' + date.getMinutes()).slice(
      -2,
    )} ${date.getDate()}/${date.getMonth() + 1}/${date.getUTCFullYear()}`;
  }

  like() {
    this.likeNumber += 1;
    this.reportService
      .likes(this.report.eventId)
      .then(result => {
        console.log(result);
      })
      .catch(error => {});
  }

  close(): void {
    this.dialogRef.close();
  }
}
