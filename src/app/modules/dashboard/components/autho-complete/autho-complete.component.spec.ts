import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthoCompleteComponent } from './autho-complete.component';

describe('AuthoCompleteComponent', () => {
  let component: AuthoCompleteComponent;
  let fixture: ComponentFixture<AuthoCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthoCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthoCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
