import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule, MatIconModule, MatDialogModule } from '@angular/material';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatMenuModule } from '@angular/material/menu';
import { MatFormFieldModule, MatInputModule } from '@angular/material';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardPageComponent } from './pages/dashboard-page/dashboard-page.component';
import { DashboardNavigationComponent } from './components/dashboard-navigation/dashboard-navigation.component';
import { ToolBarDashboardComponent } from './components/tool-bar-dashboard/tool-bar-dashboard.component';
import { MapDashboardComponent } from './components/map-dashboard/map-dashboard.component';

import { SideNavService } from './sideNav.service';
import { AgmCoreModule } from '@agm/core';
import { SignInDashboardComponent } from './components/sign-in-dashboard/sign-in-dashboard.component';
import { SignUpDashboardComponent } from './components/sign-up-dashboard/sign-up-dashboard.component';
import { AuthentificationDashboardComponent } from './components/authentification-dashboard/authentification-dashboard.component';
import { BorneDialogComponent } from './components/borne-dialog/borne-dialog.component';
import { RegisterDashboardComponent } from './components/register-dashboard/register-dashboard.component';
import { ProfileDashboardComponent } from './components/profile-dashboard/profile-dashboard.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { MatRadioModule } from '@angular/material/radio';
import { AuthoCompleteComponent } from './components/autho-complete/autho-complete.component';
import { MatTooltipModule } from '@angular/material/tooltip';
import { ReportDialogComponent } from './components/report-dialog/report-dialog.component';
//import { MatGoogleMapsAutocompleteModule } from '@angular-material-extensions/google-maps-autocomplete';
import { CityDashboardComponent } from './components/city-dashboard/city-dashboard.component';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { LayoutModule } from '@angular/cdk/layout';
import { BornesTableComponent } from './components/bornes-table/bornes-table.component';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatTabsModule } from '@angular/material/tabs';
import { MatChipsModule } from '@angular/material/chips';
import { ReportInfoDialogComponent } from './components/report-info-dialog/report-info-dialog.component';
import { AgmJsMarkerClustererModule } from '@agm/js-marker-clusterer';

@NgModule({
  declarations: [
    DashboardPageComponent,
    DashboardNavigationComponent,
    ToolBarDashboardComponent,
    MapDashboardComponent,
    AuthentificationDashboardComponent,
    SignInDashboardComponent,
    SignUpDashboardComponent,
    BorneDialogComponent,
    RegisterDashboardComponent,
    ProfileDashboardComponent,
    DialogComponent,
    AuthoCompleteComponent,
    ReportDialogComponent,
    BornesTableComponent,
    CityDashboardComponent,
    ReportInfoDialogComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    MatToolbarModule,
    MatSidenavModule,
    MatButtonModule,
    MatListModule,
    MatIconModule,
    MatSlideToggleModule,
    MatMenuModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    TextMaskModule,
    MatProgressSpinnerModule,
    MatTooltipModule,
    MatTabsModule,
    MatRadioModule,
    MatChipsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBpodC6O-ykfYe2Xv3dyhXLk1k2uKjq5EM',
      libraries: ['places'],
    }),
    // MatGoogleMapsAutocompleteModule.forRoot(),
    MatGridListModule,
    MatCardModule,
    LayoutModule,
    MatTableModule,
    MatPaginatorModule,
    MatSortModule,
    AgmJsMarkerClustererModule,
  ],
  entryComponents: [BorneDialogComponent, DialogComponent, ReportDialogComponent, ReportInfoDialogComponent],
  providers: [SideNavService],
})
export class DashboardModule {}
