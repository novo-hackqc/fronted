import * as UserActions from './../actions/user.action';
import { IUserState, initialUserState } from '../state/user.state';

export function userReducer(state = initialUserState, action: UserActions.Actions): IUserState {
  switch (action.type) {
    case UserActions.ADD_USER:
      return {
        ...state,
        user: action.payload,
      };
    default:
      return state;
  }
}
