import * as MapActions from './../actions/map.action';

import { IMapStateState, initialMapState } from '../state/map.state';

export function userReducer(state = initialMapState, action: MapActions.Actions): IMapStateState {
  switch (action.type) {
    case MapActions.ADD_MAP:
      return {
        ...state,
        map: action.payload,
      };
    default:
      return state;
  }
}
