import { Component, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Store, select } from '@ngrx/store';
import { IAppState } from 'src/app/store/state/app.state';
import { selectCurrentUser } from 'src/app/store/selectors/user.selectors';
import { ISubscription } from 'src/app/core/models/subscription.model';
import { AuthentificationService } from 'src/app/core/services/authentification.service';

@Component({
  selector: 'app-authentification-dashboard',
  templateUrl: './authentification-dashboard.component.html',
  styleUrls: ['./authentification-dashboard.component.scss'],
})
export class AuthentificationDashboardComponent implements OnInit {
  public currentUser$ = this.store.pipe(select(selectCurrentUser));
  public currentUser: ISubscription;

  constructor(
    public fb: FormBuilder,
    private store: Store<IAppState>,
    private authentificationService: AuthentificationService,
  ) {}

  ngOnInit() {
    this.currentUser$.subscribe(user => {
      this.currentUser = user;
    });
  }

  public signIn(user) {
    //this.authentificationService.get(user);
  }
}
