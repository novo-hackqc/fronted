export interface ISubscription {
  address: IAddress;
  phoneNumber: string;
}

export interface IAddress {
  description: string;
  coordinates: ICoordinates;
}

export interface ICoordinates {
  lat: number;
  lng: number;
}
