import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { SubscriptionService } from 'src/app/core/services/subscription.service';
import { ISubscription, IAddress } from 'src/app/core/models/subscription.model';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss'],
})
export class DialogComponent implements OnInit {
  public subscriptionLoading: boolean;
  public mask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  public address: string;
  public phoneNumber: string;
  public mapLat: number;
  public mapLng: number;
  public zoneInfo: any;
  public toolTipInfo:string;
  public typeName:string;

  constructor(
    public dialogRef: MatDialogRef<DialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public subscriptionService: SubscriptionService,
  ) {
    this.subscriptionLoading = false;
    this.address = data.address;
    this.mapLng = data.lng;
    this.mapLat = data.lat;
    this.zoneInfo = data.info;
    this.getTooltipInfo(this.zoneInfo.floodableZoneType)

  }

  ngOnInit() {}

  public getTooltipInfo(zoneId: number){
    console.log(zoneId)
      switch(zoneId) {
        case 0:
          this.typeName = "Zone non inondable"
          break;
        case 1:
        case 3:
        case 5:
        case 6:
        case 8:
        {
           this.toolTipInfo =  'La zone de grand courant correspond à une zone qui, selon les probabilités, est susceptible d’être inondée une fois tous les 20 ans. Si une inondation survient une année, cela ne signifie nullement 20 ans de répit par la suite. Une récurrence de 20 ans signifie plutôt qu’il subsiste 5 chances sur 100 que la zone en question soit inondée chaque année.'
           this.typeName = "Zone de grand courant"
           break;
        } ;
        case 2:
        case 4:
        case 7:
        {
          this.toolTipInfo = 'La zone de faible courant correspond à une zone qui, selon les probabilités, est susceptible d’être inondée une fois tous les 100 ans. Si une inondation survient une année, cela ne signifie nullement 20 ans de répit par la suite. Une récurrence de 20 ans signifie plutôt qu’il subsiste 5 chances sur 100 que la zone en question soit inondée chaque année.'
          this.typeName = "Zone de faible courant"
          break;
        } ;

        case 10:
        {
          this.toolTipInfo = 'Une zone inondable est une zone qui est à risque d’inondation dans une situation où le niveau des eaux augmente grandement par rapport à la normale. Dans ce type de zone le risque d’inondation n’est toujours pas qualifié.'
          this.typeName = "Zone Inondable"
          break;
        } ;
      }
  }

  async borneSubscribe() {
    this.subscriptionLoading = true;
    let address: IAddress = {
      description: this.address,
      coordinates: {
        lat: this.mapLat,
        lng: this.mapLng,
      },
    };
    let subscription: ISubscription = {
      phoneNumber: this.phoneNumber,
      address: address,
    };

    this.subscriptionService
      .subscribe(subscription)
      .then(result => {
        this.subscriptionLoading = false;
        console.log(result);
      })
      .catch(error => {
        console.log(error);
      });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}
